﻿using iot.BIZ.API;
using iot.COMMON.Entidades;
using iot.COMMON.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace iot.ClienteWPF
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        LoginModel model;
        public MainWindow()
        {
            InitializeComponent();
            model = this.DataContext as LoginModel;
        }

        private void btnIniciarSesion_Click(object sender, RoutedEventArgs e)
        {
            Usuario u = FactoryManager.UsuarioManager().Login(model.Correo, model.Password);
            if (u!=null)
            {
                MessageBox.Show($"Bienvenido {u.Nombre}", "IoT-SPIREX", MessageBoxButton.OK, MessageBoxImage.Information);
                PanelUsuario panel = new PanelUsuario(u);
                panel.Show();
                this.Close();
            }
            else 
            {
                MessageBox.Show("Usuario y/o contraseña incorrecta", "IoT-SPIREX", MessageBoxButton.OK, MessageBoxImage.Error);            
            }

        }
    }
}
