﻿using iot.BIZ.API;
using iot.COMMON.Entidades;
using iot.COMMON.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace iot.ClienteWPF
{
    /// <summary>
    /// Lógica de interacción para PanelUsuario.xaml
    /// </summary>
    public partial class PanelUsuario : Window
    {
        PanelUsuarioModel model;
        public PanelUsuario(Usuario usuario)
        {
            InitializeComponent();
            model = this.DataContext as PanelUsuarioModel;
            model.Usuario = usuario;
            model.Dispositivos = FactoryManager.DispositivoManager().DispositivosDeUsuarioPorId(usuario.Id).ToList();
            lstDispositivos.ItemsSource = null;
            lstDispositivos.ItemsSource = model.Dispositivos;
        }

        private void lstDispositivos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            model.DispositivoSeleccionado = lstDispositivos.SelectedItem as Dispositivo;
            model.LecturasDelDispositivo = FactoryManager.LecturaManager().LecturasDelDispositivo(model.DispositivoSeleccionado.Id).ToList();
            dtgLecturas.ItemsSource = null;
            dtgLecturas.ItemsSource = model.LecturasDelDispositivo;
        }
    }
}
