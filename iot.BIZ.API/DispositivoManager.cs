﻿using iot.COMMON.Entidades;
using iot.COMMON.Interfaces;
using iot.COMMON.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iot.BIZ.API
{
    public class DispositivoManager : GenericManager<Dispositivo>, IDispositivoManager
    {
        private async Task<Dispositivo> DispositivoPerteneceAUsuarioAsync(string idDispositivo, string idUsuario)
        {
            ConusltaAPIModel model = new ConusltaAPIModel()
            {
                NombreMetodo = "DispositivoPerteneceAUsuario",
                Parametros = new List<string>() { idDispositivo, idUsuario }
            };
            var r = await TraerDatos(model).ConfigureAwait(false); ;
            var result=  r.ToList();
            return result[0];
        }

        private async Task<IEnumerable<Dispositivo>> DispositivosDeUsuarioPorEmailAsync(string email)
        {
            ConusltaAPIModel model = new ConusltaAPIModel()
            {
                NombreMetodo = "DispositivosDeUsuarioPorEmail",
                Parametros = new List<string>() { email }
            };
            return await TraerDatos(model).ConfigureAwait(false); ;
        }

        private async Task<IEnumerable<Dispositivo>> DispositivosDeUsuarioPorIdAsync(string id)
        {
            ConusltaAPIModel model = new ConusltaAPIModel()
            {
                NombreMetodo = "DispositivosDeUsuarioPorId",
                Parametros = new List<string>() { id }
            };
            return await TraerDatos(model).ConfigureAwait(false); ;
        }

        public Dispositivo DispositivoPerteneceAUsuario(string idDispositivo, string idUsuario)
        {
            return DispositivoPerteneceAUsuarioAsync(idDispositivo, idUsuario).Result;
        }

        public IEnumerable<Dispositivo> DispositivosDeUsuarioPorEmail(string email)
        {
            return DispositivosDeUsuarioPorEmailAsync(email).Result;
        }

        public IEnumerable<Dispositivo> DispositivosDeUsuarioPorId(string id)
        {
            return DispositivosDeUsuarioPorIdAsync(id).Result;
        }
    }
}
