﻿using iot.COMMON.Entidades;
using iot.COMMON.Interfaces;
using iot.COMMON.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iot.BIZ.API
{
    public class UsuarioManager : GenericManager<Usuario>, IUsuarioManager
    {
        private async Task<Usuario> LoginAsync(string email, string password)
        {
            ConusltaAPIModel model = new ConusltaAPIModel()
            {
                NombreMetodo = "Login",
                Parametros = new List<string>() { email, password }
            };
            var r = await TraerDatos(model).ConfigureAwait(false);
            var result = r.ToList();
            return result[0];
        }
        public Usuario Login(string email, string password)
        {
            return LoginAsync(email, password).Result;
        }
    }
}
