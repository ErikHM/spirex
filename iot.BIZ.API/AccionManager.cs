﻿using iot.COMMON.Entidades;
using iot.COMMON.Interfaces;
using iot.COMMON.Modelos;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace iot.BIZ.API
{
    public class AccionManager : GenericManager<Accion>, IAccionManager
    {
        private async Task<IEnumerable<Accion>> AccionesDelDispositivoAsync(string id)
        {
            ConusltaAPIModel model = new ConusltaAPIModel()
            {
                NombreMetodo = "AccionesDelDispositivo",
                Parametros = new List<string>() { id }
            };
            return await TraerDatos(model).ConfigureAwait(false); ;
        }

        private async Task<IEnumerable<Accion>> AccionesDelDispositivoAsync(string id, DateTime inicio, DateTime fin)
        {
            ConusltaAPIModel model = new ConusltaAPIModel()
            {
                NombreMetodo = "AccionesDelDispositivo",
                Parametros = new List<string>() { id, inicio.ToString(), fin.ToString() }
            };
            return await TraerDatos(model).ConfigureAwait(false); ;
        }


        private async Task<IEnumerable<Accion>> AccionesDelDispositivoAsync(string id, string actuador, DateTime inicio, DateTime fin)
        {
            ConusltaAPIModel model = new ConusltaAPIModel()
            {
                NombreMetodo = "AccionesDelDispositivo",
                Parametros = new List<string>() { id,actuador, inicio.ToString(), fin.ToString() }
            };
            return await TraerDatos(model).ConfigureAwait(false); ;
        }

   

        public IEnumerable<Accion> AccionesDelDispositivo(string id)
        {
            return AccionesDelDispositivoAsync(id).Result;
        }

        public IEnumerable<Accion> AccionesDelDispositivo(string id, DateTime inicio, DateTime fin)
        {
            return AccionesDelDispositivoAsync(id, inicio, fin).Result;
        }

        public IEnumerable<Accion> AccionesDelDispositivo(string id, string actuador, DateTime inicio, DateTime fin)
        {
            return AccionesDelDispositivoAsync(id, actuador, inicio, fin).Result;
        }
    }
}
