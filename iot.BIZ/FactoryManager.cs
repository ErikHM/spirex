﻿using iot.COMMON.Entidades;
using iot.COMMON.Interfaces;
using iot.COMMON.Validadores;
using iot.DAL;
using System;
using System.Collections.Generic;
using System.Text;

namespace iot.BIZ
{
    public static class FactoryManager
    {
        public static UsuarioManager UsuarioManager()
        {
            return new UsuarioManager(new GenericRepository<Usuario>(new UsuarioValidator()));
        }

        public static IAccionManager AccionManager()
        {
            return new AccionManager(new GenericRepository<Accion>(new AccionValidator()));
        }
        public static ILecturaManager LecturaManager()
        {
            return new LecturaManager(new GenericRepository<Lectura>(new LecturaValidator()));
        }

        public static IDispositivoManager DispositivoManager()
        {
            return new DispositivoManager(new GenericRepository<Dispositivo>(new DispositivoValidator()), new GenericRepository<Usuario>(new UsuarioValidator()));
        }
    }
}
