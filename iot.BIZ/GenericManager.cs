﻿using iot.COMMON.Entidades;
using iot.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace iot.BIZ
{
    public abstract class GenericManager<T> : IGenericManager<T> where T : BaseDTO
    {
        internal IGenericRepository<T> repository;
        public GenericManager(IGenericRepository<T> repository)
        {
            this.repository = repository;
        }
        public IEnumerable<T> ObtenerTodos => repository.Read;

        public string Error => repository.Error;

        public T Actualizar(T entidad)
        {

            return repository.Update(entidad);
        }

        public T BuscarPorId(string id)
        {
            return repository.SearchById(id);
        }

        public bool Eliminar(string id)
        {
            return repository.Delete(id);
        }

        public bool EliminarVarios(List<T> elementosAEliminar)
        {
            foreach (T item in elementosAEliminar)
            {
                if (!Eliminar(item.Id))
                {
                    return false;
                }
            }
            return true;
        }

        public T Insertar(T entidad)
        {
            return repository.Create(entidad);
        }
    }
}
