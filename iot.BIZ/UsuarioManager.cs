﻿using iot.COMMON.Entidades;
using iot.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iot.BIZ
{
    public class UsuarioManager : GenericManager<Usuario>, IUsuarioManager
    {
        public UsuarioManager(IGenericRepository<Usuario> repository) : base(repository)
        {
        }

        public Usuario Login(string email, string password)
        {
            return repository.Query(u => u.Correo == email && u.Password == password).ToList().SingleOrDefault();
        }
    }
}
