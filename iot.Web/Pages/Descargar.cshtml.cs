﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iot.BIZ;
using iot.COMMON.Entidades;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace iot.Web.Pages
{
    public class DescargarModel : PageModel
    {
        [BindProperty]
        public Dispositivo Dispositivo { get; set; }
        [BindProperty]
        public List<Lectura> Lecturas { get; set; }
        [BindProperty]
        public Usuario Usuario { get; set; }
        [BindProperty]
        public List<Usuario> Usuarios { get; set; }
        public IActionResult OnGet(string id)
        {
            Dispositivo = FactoryManager.DispositivoManager().BuscarPorId(id);
            Lecturas = FactoryManager.LecturaManager().LecturasDelDispositivo(id).ToList();

            var builder = new StringBuilder();
            builder.AppendLine("idDispositivo,MQ2");
            foreach (var lec in Lecturas)
            {
                builder.AppendLine($"{lec.IdDispositivo},{lec.MQ2}");
            }
            return File(Encoding.UTF8.GetBytes(builder.ToString()), "text/csv", "Lecturas.csv");
        }
    }
}