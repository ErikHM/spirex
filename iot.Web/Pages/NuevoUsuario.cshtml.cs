﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using iot.BIZ;
using iot.COMMON.Entidades;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace iot.Web.Pages
{
    public class NuevoUsuarioModel : PageModel
    {
        [BindProperty]
        public Usuario Usuario { get; set; }
        [BindProperty]
        public string Mensaje { get; set; }
        [BindProperty]
        public List<Usuario> Usuariol { get; set; }


        //public void OnGet(string id)
        //{
        //    Usuario = new Usuario();
        //    Usuario.Id = id;
        //    Mensaje = "";
        //}

        public void OnGet(string id)
        {

            Usuario = new Usuario();
            Mensaje = "";

            Random r = new Random();
            Random l = new Random();
            for (int i = 0; i < r.Next(1, 2); i++)
            {
                int U = l.Next(1, 10001);
                FactoryManager.UsuarioManager().Insertar(new Usuario()
                {
                     Nombre = "User " + U,
                     Correo = "U " + "User@gamil.com",
                     Password = "user " + U
                });
            }
            //while (Dispositivos.Count<=10)
            //{
            //    Dispositivos.Add(new Dispositivo()
            //    {
            //        IdUsuario = Dispositivo.IdUsuario,
            //        Ubicacion = "Casa" + Dispositivos.Count,
            //        UsoRelevador1 = "Ventilador" + Dispositivos.Count,
            //        UsoRelevador2 = "Ventilador" + Dispositivos.Count,
            //        UsoRelevador3 = "Ventilador" + Dispositivos.Count,
            //        UsoRelevador4 = "Ventilador" + Dispositivos.Count,
            //        UsoBuzzer = "Alarma" + Dispositivos.Count
            //    });
            //}

            //Usuariol = FactoryManager.UsuarioManager().Eliminar(id).ToString();

            //Inserccion de lecturas


        }

        //public async Task<ActionResult> OnPost(Dispositivo id)
        //{
        //    Random r = new Random();
        //    Random l = new Random();
        //    int U = l.Next(1,10001)
        //    FactoryManager.DispositivoManager().Insertar(new Dispositivo()
        //    {
        //        IdUsuario = id,
        //        Ubicacion = "Casa" + U,
        //        UsoRelevador1 = "Ventilador" + U,
        //        UsoRelevador2 = "Ventilador" + U,
        //        UsoRelevador3 = "Ventilador" + U,
        //        UsoRelevador4 = "Ventilador" + U,
        //        UsoBuzzer = "Alarma" + U
        //    });

        //    Dispositivo = FactoryManager.DispositivoManager().Insertar(id);
        //}



        public async Task<ActionResult> OnPost()
        {
            if (FactoryManager.UsuarioManager().Insertar(Usuario) != null)
            {
                return RedirectToPage("/PanelUsuario");
            }
            else
            {
                Mensaje = "Error al crear el Usuario";
                return Page();
            }
        }
    }
}
