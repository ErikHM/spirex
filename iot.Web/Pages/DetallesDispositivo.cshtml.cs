﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iot.BIZ;
using iot.COMMON.Entidades;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using OpenNETCF.MQTT;

namespace iot.Web.Pages
{
    [Authorize]
    public class DetallesDispositivoModel : PageModel
    {
        [BindProperty]
        public Dispositivo Dispositivo { get; set; }
        [BindProperty]
        public List<Lectura> Lecturas { get; set; }

        public void OnGet(string id)
        {
            Dispositivo = FactoryManager.DispositivoManager().BuscarPorId(id);
//#if DEBUG
//            Random r = new Random();
//            Random l = new Random();
//            for (int i = 0; i < r.Next(1, 100); i++)
//            {
//                int L = l.Next(0, 3);
//                if (L==1)
//                {
//                    FactoryManager.LecturaManager().Insertar(new Lectura()
//                    {
                      
//                        IdDispositivo = id,
//                        MQ2 = "Gas o Humo Detectado"
//                    });
//                }
//                else
//                {
//                    FactoryManager.LecturaManager().Insertar(new Lectura()
//                    {

//                        IdDispositivo = id,
//                        MQ2 = "Gas o Humo no Detectado"
//                    });
//                }
//                //FactoryManager.LecturaManager().Insertar(new Lectura()
//                //{

//                //    IdDispositivo = id,
//                //    MQ2 = "Gas o Humo Detectado" 
//                //});
//            }
//#endif
            Lecturas = FactoryManager.LecturaManager().LecturasDelDispositivo(id).ToList();

            
        }

        public IActionResult CSV(string id)
        {
            Dispositivo = FactoryManager.DispositivoManager().BuscarPorId(id);
            Lecturas = FactoryManager.LecturaManager().LecturasDelDispositivo(id).ToList();

            var builder = new StringBuilder();
            builder.AppendLine("idDispositivo,MQ2");
            foreach (var lec in Lecturas)
            {
                builder.AppendLine($"{lec.IdDispositivo},{lec.MQ2}");
            }
            return File(Encoding.UTF8.GetBytes(builder.ToString()), "text/csv", "Lecturas.csv");
        }

    }
}