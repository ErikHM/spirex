﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using iot.BIZ;
using iot.COMMON.Entidades;
using iot.COMMON.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace iot.Web.Pages
{
    public class PobladorModel : PageModel
    {
        [BindProperty]
        public Dispositivo Dispositivo { get; set; }
        [BindProperty]
        public Usuario Usuario { get; set; }
        [BindProperty]
        public string Mensaje { get; set; }
        [BindProperty]
        public List<Lectura> Lecturas { get; set; }
        [BindProperty]
        public List<Dispositivo> Dispositivos { get; set; }
        IUsuarioManager _usuarioManager;
        IDispositivoManager _dispositivoManager;
        ILecturaManager _lecturaManager;
        public void OnGet()
        {
            //Usuario = FactoryManager.UsuarioManager().BuscarPorId(id);
            try
            {
                _usuarioManager = FactoryManager.UsuarioManager();
                _dispositivoManager = FactoryManager.DispositivoManager();
                _lecturaManager = FactoryManager.LecturaManager();
                Random r = new Random();
                Random l = new Random();
                _usuarioManager.EliminarVarios(_usuarioManager.ObtenerTodos.ToList());
                _dispositivoManager.EliminarVarios(_dispositivoManager.ObtenerTodos.ToList());
                _lecturaManager.EliminarVarios(_lecturaManager.ObtenerTodos.ToList());


                //Maneja usuarios
                for (int i = 0; i < 1500; i++)
                {
                    if (_usuarioManager.Insertar(new Usuario()
                    {
                        Nombre = "UserPoblador " + i,
                        Correo = $"User{i}@gmail.com",
                        Password = "user" + i
                    }) is Usuario usuario)
                    {
                        int cantidadDispositivos = r.Next(7, 10);
                        for (int j = 0; j < cantidadDispositivos; j++)
                        {
                            if (_dispositivoManager.Insertar(new Dispositivo()
                            {
                                IdUsuario = usuario.Id,
                                Ubicacion = "Casa" + j,
                                UsoRelevador1 = "Ventilador" + j,
                                UsoRelevador2 = "Ventilador" + j,
                                UsoRelevador3 = "Ventilador" + j,
                                UsoRelevador4 = "Ventilador" + j,
                                UsoBuzzer = "Alarma" + j
                            }) is Dispositivo dispositivo)
                            {
                                for (int d = 0; d < 30; d++)
                                {
                                    if (_lecturaManager.Insertar(new Lectura
                                    {
                                        IdDispositivo = dispositivo.Id,
                                        MQ2 = r.NextDouble() > 0.5 ? "Gas Detectado" : "Gas No Detectado"
                                    }) is null)
                                    {
                                        throw new Exception(_lecturaManager.Error);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            catch (Exception)
            {

                throw;
            }
        }
    }
}