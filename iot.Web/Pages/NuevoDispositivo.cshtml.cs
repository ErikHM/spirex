﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using iot.BIZ;
using iot.COMMON.Entidades;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace iot.Web.Pages
{
    [Authorize]
    public class NuevoDispositivoModel : PageModel
    {
        [BindProperty]
        public Dispositivo Dispositivo { get; set; }
        [BindProperty]
        public Usuario Usuario { get; set; }
        [BindProperty]
        public string Mensaje { get; set; }
        [BindProperty]
        public List<Lectura> Lecturas { get; set; }
        [BindProperty]
        public List<Dispositivo> Dispositivos { get; set; }

        //public void OnGet(string id)
        //{
        //    Dispositivo = new Dispositivo();
        //    Dispositivo.IdUsuario = id;
        //    Mensaje = "";
        //}

        public void OnGet(string id)
        {
            Usuario = FactoryManager.UsuarioManager().BuscarPorId(id);

            Dispositivo = new Dispositivo();
            Dispositivo.IdUsuario = id;
            Mensaje = "";

            //Random r = new Random();
            //Random l = new Random();
            //for (int i = 0; i < r.Next(1, 2); i++)
            //{
            //    int U = l.Next(1, 10001);
            //    FactoryManager.DispositivoManager().Insertar(new Dispositivo()
            //    {
            //        IdUsuario = id,
            //        Ubicacion = "Casa" + U,
            //        UsoRelevador1 = "Ventilador" + U,
            //        UsoRelevador2 = "Ventilador" + U,
            //        UsoRelevador3 = "Ventilador" + U,
            //        UsoRelevador4 = "Ventilador" + U,
            //        UsoBuzzer = "Alarma" + U
            //        //IdUsuario = id,
            //        //Ubicacion = "Casa Gra 2",
            //        //UsoRelevador1 = "Ventilador Gra",
            //        //UsoRelevador2 = "Ventilador Gra",
            //        //UsoRelevador3 = "Ventilador Gra",
            //        //UsoRelevador4 = "Ventilador Gra",
            //        //UsoBuzzer = "Alarma Gra"
            //    });

            //    Dispositivo = FactoryManager.DispositivoManager().BuscarPorId(id);
            //    Random g = new Random();
            //    Random e = new Random();
            //    for (int n = 0; n < g.Next(1, 100); i++)
            //    {
            //        int L = e.Next(0, 3);
            //        if (L == 1)
            //        {
            //            FactoryManager.LecturaManager().Insertar(new Lectura()
            //            {

            //                IdDispositivo = id,
            //                MQ2 = "Gas o Humo Detectado"
            //            });
            //        }
            //        else
            //        {
            //            FactoryManager.LecturaManager().Insertar(new Lectura()
            //            {

            //                IdDispositivo = id,
            //                MQ2 = "Gas o Humo no Detectado"
            //            });
            //        }
            //    }

            //    Lecturas = FactoryManager.LecturaManager().LecturasDelDispositivo(id).ToList();
            //}
            //while (Dispositivos.Count<=10)
            //{
            //    Dispositivos.Add(new Dispositivo()
            //    {
            //        IdUsuario = Dispositivo.IdUsuario,
            //        Ubicacion = "Casa" + Dispositivos.Count,
            //        UsoRelevador1 = "Ventilador" + Dispositivos.Count,
            //        UsoRelevador2 = "Ventilador" + Dispositivos.Count,
            //        UsoRelevador3 = "Ventilador" + Dispositivos.Count,
            //        UsoRelevador4 = "Ventilador" + Dispositivos.Count,
            //        UsoBuzzer = "Alarma" + Dispositivos.Count
            //    });
            //}

            //Dispositivos = FactoryManager.DispositivoManager().DispositivosDeUsuarioPorId(id).ToList();

            //Inserccion de lecturas


        } 

        //public async Task<ActionResult> OnPost(Dispositivo id)
        //{
        //    Random r = new Random();
        //    Random l = new Random();
        //    int U = l.Next(1,10001)
        //    FactoryManager.DispositivoManager().Insertar(new Dispositivo()
        //    {
        //        IdUsuario = id,
        //        Ubicacion = "Casa" + U,
        //        UsoRelevador1 = "Ventilador" + U,
        //        UsoRelevador2 = "Ventilador" + U,
        //        UsoRelevador3 = "Ventilador" + U,
        //        UsoRelevador4 = "Ventilador" + U,
        //        UsoBuzzer = "Alarma" + U
        //    });

          //  Dispositivo = FactoryManager.DispositivoManager().Insertar(id);
        //}



        public async Task<ActionResult> OnPost()
        {
            if (FactoryManager.DispositivoManager().Insertar(Dispositivo) != null)
            {
                return RedirectToPage("/PanelUsuario", new { idUsuario = Dispositivo.IdUsuario });
            }
            else
            {
                Mensaje = "Error al crear el dispositivo";
                return Page();
            }
        }
    }
}