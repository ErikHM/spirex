﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using iot.BIZ;
using iot.COMMON.Entidades;
using iot.COMMON.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace iot.Web.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class IngresaDatosController : ControllerBase
    {
        IUsuarioManager usuarioManager;
        IDispositivoManager dispositivoManager;
        ILecturaManager lecturaManager;
        IAccionManager accionManager;
        public IngresaDatosController()
        {
            usuarioManager = FactoryManager.UsuarioManager();
            dispositivoManager = FactoryManager.DispositivoManager();
            lecturaManager = FactoryManager.LecturaManager();
            accionManager = FactoryManager.AccionManager();
        }

        [HttpPut]
        public ActionResult<Accion> Put(string correo, string password, string idDispositivo, string actuador, bool estado)
        {
            Usuario u = usuarioManager.Login(correo, password);
            if (u != null)
            {
                if (dispositivoManager.DispositivoPerteneceAUsuario(idDispositivo, u.Id) != null)
                {
                    try
                    {
                        return Ok(accionManager.Insertar(new Accion()
                        {
                            Actuador = actuador,
                            Estado = estado,
                            IdDispositivo = idDispositivo
                        }));
                    }
                    catch (Exception)
                    {
                        return BadRequest("No se pudo crear la accion" + lecturaManager.Error);
                    }
                }
                else
                {
                    return BadRequest("Dispositivo no pertenece al usuario " );
                }
            }
            else
            {
                return BadRequest("Usuario y/o contraseña incorrecta");
            }
        }
        // POST: api/IngresaDatos
        [HttpPost]
        public ActionResult<Lectura> Post(string correo, string password, string idDispositivo, string MQ2)
        {
            Usuario u = usuarioManager.Login(correo, password);
            if (u!=null)
            {
                if (dispositivoManager.DispositivoPerteneceAUsuario(idDispositivo, u.Id)!=null)
                {
                    try
                    {
                        return Ok(lecturaManager.Insertar(new Lectura()
                        {
                            MQ2 = MQ2,
                            IdDispositivo = idDispositivo
                        }));
                    }
                    catch (Exception)
                    {
                        return BadRequest("No se pudo crear la lectura");
                    }
                }
                else
                {
                    return BadRequest("Dispositivo no pertenece al usuario " + lecturaManager.Error);
                }
            }
            else
            {
                return BadRequest("Usuario y/o contraseña incorrecta");
            }
        }
    }
}
