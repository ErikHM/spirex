﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using iot.BIZ;
using iot.COMMON.Entidades;
using iot.COMMON.Interfaces;
using iot.COMMON.Modelos;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace iot.Web.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class LecturaController : GenericAPIController<Lectura>
    {
        static ILecturaManager manager = FactoryManager.LecturaManager();
        public LecturaController() : base(manager)
        {
        }

        public override ActionResult<IEnumerable<Lectura>> Consulta([FromBody] ConusltaAPIModel model, string id)
        {
            List<Lectura> datos = null;
            try
            {
                if (model.NombreMetodo == "LecturasDelDispositivo")
                {
                    switch (model.Parametros.Count)
                    {
                        case 1:
                            datos = manager.LecturasDelDispositivo(model.Parametros[0]).ToList();
                            break;
                        case 3:
                            datos = manager.LecturasDelDispositivo(model.Parametros[0], DateTime.Parse(model.Parametros[1]),DateTime.Parse(model.Parametros[2])).ToList();
                            break;
                        default:
                            datos = null;
                            break;
                    }
                    if (datos==null)
                    {
                        return BadRequest("Numero de parametros incorectos");
                    }
                    else
                    {
                        return Ok(datos);
                    }
                    
                }
                else
                {
                    return BadRequest("Nombre del Metodo no encontrado");
                }

            }
            catch (Exception)
            {
                return BadRequest("Error al procesar los datos");
            }
        }
    }
}