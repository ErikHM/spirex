﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using iot.BIZ;
using iot.COMMON.Entidades;
using iot.COMMON.Interfaces;
using iot.COMMON.Modelos;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace iot.Web.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class DispositivoController : GenericAPIController<Dispositivo>
    {
        static IDispositivoManager manager = FactoryManager.DispositivoManager();
        public DispositivoController() : base(manager)
        {
        }

        public override ActionResult<IEnumerable<Dispositivo>> Consulta([FromBody] ConusltaAPIModel model, string id)
        {
            List<Dispositivo> datos;
            try
            {
                switch (model.NombreMetodo)
                {
                    case "DispositivoPerteneceAUsuario":
                        datos = new List<Dispositivo>();
                        datos.Add(manager.DispositivoPerteneceAUsuario(model.Parametros[0], model.Parametros[1]));
                        break;
                    case "DispositivosDeUsuarioPorEmail":
                        datos=manager.DispositivosDeUsuarioPorEmail(model.Parametros[0]).ToList();
                        break;
                    case "DispositivosDeUsuarioPorId":
                        datos = manager.DispositivosDeUsuarioPorId(model.Parametros[0]).ToList();
                        break;
                    default:
                        datos = null;
                        break;
                }
                if (datos==null)
                {
                    return Ok("Nombre del metodo no encontrado");
                }
                else
                {
                    return Ok(datos);
                }
            }
            catch (Exception)
            {
                return BadRequest("Error al procesar los datos");
            }
        }
    }
}