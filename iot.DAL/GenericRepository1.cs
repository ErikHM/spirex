﻿using FluentValidation;
using FluentValidation.Results;
using iot.COMMON.Entidades;
using iot.COMMON.Interfaces;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
//using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;
using System.Text;


namespace iot.DAL
{
    public class GenericRepository1<T> : IGenericRepository<T> where T: BaseDTO
    {
        MongoClient _client;
        IMongoDatabase _db;
        AbstractValidator<T> _validator;
        public GenericRepository1()
        {
            _client = new MongoClient(new MongoUrl("mongodb://EBIX:G@briela17@ds036789.mlab.com:36789/spirex?retryWrites=false"));
            _db = _client.GetDatabase("spirex");
        }
        private IMongoCollection<T> Collection() => _db.GetCollection<T>(typeof(T).Name);
        public string Error { get; private set; }

        public IEnumerable<T> Read 
        { 
            get
            {
                try
                {
                    IEnumerable<T> datos = Collection().AsQueryable();
                    return datos == null ? throw new Exception("Error en la conexión") : datos;
                }
                catch (Exception ex)
                {
                    Error = ex.Message;
                    return null;
                }
            }
        }

        public T Create(T entidad)
        {
            try
            {
                entidad.Id = ObjectId.GenerateNewId().ToString();
                entidad.FechaHora = DateTime.Now;
                //bool valido = true;
                //ValidationResult validationResult = null;

                 var validationResult = _validator.Validate(entidad);
                //valido = ValidationResult.IsValid;
                if (validationResult.IsValid)
                {
                    Collection().InsertOne(entidad);
                    Error = "";
                    return entidad;
                }
                else
                {
                    Error = "Error de Validación\n";
                    foreach (var item in validationResult.Errors)
                    {
                        Error += item.ErrorMessage + ". \n";
                    }
                    return null;
                }
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return null;
            }
        }

        public bool Delete(string id)
        {
            try
            {
                int r = (int)Collection().DeleteOne(entidad => entidad.Id == id).DeletedCount;
                Error = r == 1 ? "" : "No se elimino";
                return r == 1;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return false;
            }
        }

        public IEnumerable<T> Query(Expression<Func<T, bool>> predicado)
        {
            return Read.Where(predicado.Compile());
        }

        public T SearchById(string id)
        {
            return Read.Where(entidad => entidad.Id == id).SingleOrDefault();
        }

        public T Update(T entidad)
        {
            try
            {
                bool valido = true;
                ValidationResult validationResult = null;

                validationResult = _validator.Validate(entidad);
                valido = validationResult.IsValid;
                if (valido)
                {
                    int r = (int)Collection().ReplaceOne(e => e.Id == entidad.Id, entidad).ModifiedCount;
                    Error = "";
                    return r == 1 ? entidad : null;
                }
                else
                {
                    Error = "Error de validación:\n";
                    foreach (var item in validationResult.Errors)
                    {
                        Error += item.ErrorMessage + ".\n";
                    }
                    return null;
                }
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return null;
            }
        }
    }
}
