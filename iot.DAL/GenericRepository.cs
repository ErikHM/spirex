﻿using iot.COMMON.Entidades;
using iot.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using MongoDB.Bson;
using LiteDB;
using FluentValidation;
using System.Linq;
using MongoDB.Driver;
using FluentValidation.Results;

namespace iot.DAL
{
    public class GenericRepository<T> : IGenericRepository<T> where T : BaseDTO
    {
        MongoClient _client;
        IMongoDatabase _db;
        AbstractValidator<T> _validator;

        public GenericRepository(AbstractValidator<T> validator)
        {

            _client = new MongoClient(new MongoUrl("mongodb://EBIX:SPIREX123@ds036789.mlab.com:36789/spirex?retryWrites=false"));
            //_client = new MongoClient(new MongoUrl("mongodb://35.236.101.255:27017?retryWrites=false"));
            _db = _client.GetDatabase("spirex");  //mongodb://<dbuser>:<dbpassword>@ds036789.mlab.com:36789/spirex
            _validator = validator;

        }
        private IMongoCollection<T> Collection() => _db.GetCollection<T>(typeof(T).Name);
        public string Error { get; private set; }

        public IEnumerable<T> Read
        {
            get
            {
                try
                {
                    IEnumerable<T> datos = Collection().AsQueryable();
                    return datos == null ? throw new Exception("Error en la conexión") : datos;
                }
                catch (Exception ex)
                {
                    Error = ex.Message;
                    return null;
                }
            }
        }

        public T Create(T entidad)
        {
            try
            {
                entidad.Id = MongoDB.Bson.ObjectId.GenerateNewId().ToString();
                entidad.FechaHora = DateTime.Now;
                //bool valido = true;
                //ValidationResult validationResult = null;

                var validationResult = _validator.Validate(entidad);
                //valido = ValidationResult.IsValid;
                if (validationResult.IsValid)
                {
                    Collection().InsertOne(entidad);
                    Error = "";
                    return entidad;
                }
                else
                {
                    Error = "Error de Validación\n";
                    foreach (var item in validationResult.Errors)
                    {
                        Error += item.ErrorMessage + ". \n";
                    }
                    return null;
                }
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return null;
            }
        }

        public bool Delete(string id)
        {
            try
            {
                int r = (int)Collection().DeleteOne(entidad => entidad.Id == id).DeletedCount;
                Error = r == 1 ? "" : "No se elimino";
                return r == 1;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return false;
            }
        }

        public IEnumerable<T> Query(Expression<Func<T, bool>> predicado)
        {
            return Read.Where(predicado.Compile());
        }

        public T SearchById(string id)
        {
            return Read.Where(entidad => entidad.Id == id).SingleOrDefault();
        }

        public T Update(T entidad)
        {
            try
            {
                bool valido = true;
                ValidationResult validationResult = null;

                validationResult = _validator.Validate(entidad);
                valido = validationResult.IsValid;
                if (valido)
                {
                    int r = (int)Collection().ReplaceOne(e => e.Id == entidad.Id, entidad).ModifiedCount;
                    Error = "";
                    return r == 1 ? entidad : null;
                }
                else
                {
                    Error = "Error de validación:\n";
                    foreach (var item in validationResult.Errors)
                    {
                        Error += item.ErrorMessage + ".\n";
                    }
                    return null;
                }
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return null;
            }
        }
    }
}
