﻿using FluentValidation;
using iot.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace iot.COMMON.Validadores
{
    public class UsuarioValidator:GenericValidator<Usuario>
    {
        public UsuarioValidator()
        {
            RuleFor(u => u.Correo).NotNull().NotEmpty().EmailAddress();
            RuleFor(u => u.Password).NotEmpty().NotNull().Length(5, 50);
            RuleFor(u => u.Nombre).NotNull().NotEmpty().Length(10, 20);
        }
    }
}
