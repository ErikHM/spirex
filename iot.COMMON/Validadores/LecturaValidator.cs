﻿using FluentValidation;
using iot.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace iot.COMMON.Validadores
{
    public class LecturaValidator:GenericValidator<Lectura>
    {
        public LecturaValidator()
        {
            RuleFor(l => l.MQ2).NotNull().NotEmpty();
            RuleFor(l => l.IdDispositivo).NotNull().NotEmpty();
        }
    }
}
