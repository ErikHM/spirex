﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iot.COMMON.Entidades
{
    public class Lectura: BaseDTO
    {
        public string IdDispositivo { get; set; }
        public string MQ2 { get; set; }
    }
}
