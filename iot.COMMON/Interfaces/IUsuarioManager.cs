﻿using iot.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace iot.COMMON.Interfaces
{
    public interface IUsuarioManager : IGenericManager<Usuario>
    {
        Usuario Login(string email, string password);
    }
}
