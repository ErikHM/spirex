﻿using iot.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace iot.COMMON.Interfaces
{
    public interface IGenericManager<T> where T:BaseDTO
    {
        string Error { get; }
        IEnumerable<T> ObtenerTodos { get; }
        T Insertar(T entidad);
        T Actualizar(T entidad);
        bool Eliminar(string id);
        bool EliminarVarios(List<T> elementosAEliminar);
        T BuscarPorId(string id);

    }
}
