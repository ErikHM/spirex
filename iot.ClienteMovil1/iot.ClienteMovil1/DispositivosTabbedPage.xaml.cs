﻿using iot.BIZ.API;
using iot.COMMON.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace iot.ClienteMovil1
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DispositivosTabbedPage : TabbedPage
    {
        PanelUsuarioModel model;
        public DispositivosTabbedPage(PanelUsuarioModel model)
        {
            InitializeComponent();
            this.model = model;
            this.BindingContext = model;
            model.LecturasDelDispositivo = FactoryManager.LecturaManager().LecturasDelDispositivo(model.DispositivoSeleccionado.Id).ToList();
            lstLecturas.ItemsSource = null;
            lstLecturas.ItemsSource = model.LecturasDelDispositivo;

        }
    }
}