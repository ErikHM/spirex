﻿using iot.BIZ.API;
using iot.COMMON.Entidades;
using iot.COMMON.Modelos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace iot.ClienteMovil1
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        LoginModel model;
        public MainPage()
        {
            InitializeComponent();
            model = this.BindingContext as LoginModel;
        }

        private void btnIniciarSesion_Clicked(object sender, EventArgs e)
        {
            Usuario u = FactoryManager.UsuarioManager().Login(model.Correo, model.Password);
            if (u != null)
            {
                DisplayAlert("SPIREX -IoT", $"Bienvenido {u.Nombre}", "Ok");
                Navigation.PushAsync(new PanelUsuario(u));
            }
            else
            {
                DisplayAlert("SPIREX -IoT", "Usuario y/o contraseña incorrecta", "Ok");
            }
        }
    }
}
